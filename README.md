# Description
ในส่วนนี้จะเป็นส่วนของ frontend 

# Quick Test
หากอยากจะลอง เทสโค้ด ก็รันคำสั่งตามนี้ได้เลย <br>
ซึ่ง ถ้าอยากรู้ว่า เชื่อมต่อ api ได้จริงไหม ให้ไปที่ช่อง url ละ search ว่า <br>
/getTable ซึ่งถ้าเชื่อมต่อได้ เดียวข้อมูล json มันจะขึ้นมาเองที่หน้า browser <br>
```bash
# clone & cd
git clone https://gitlab.com/zergreen/os-y2-backend.git
cd os-y2-backend

# install package
npm i

# run backend server
npm start
```

# FAQ
1. ให้สร้าง database ขึ้นมาก่อน ละทำการเชื่อมกับ database นั้นซ่ะ
โดย script การสร้าง table จะอยู่ใน dumb_sql ไดเรคทอรี่ <br>
ละก็ ใน config/db.js ตัวนี้มัน config ไว้ที่ database ของผู้เขียน ให้ผู้อ่านทำการ config เป็น database ของตัวเองส่ะ  <br>
ซึ่ง mysql สามารถหาอ่านวิธีติดตั้งได้จาก [mysql](https://tasty-feet-485.notion.site/Introduction-to-MVC-Comsci-KMITL-654b17725ca942ca8491a649b6fc8d5d) นี้

# Learn
ในส่วนนี้จะเป็นลิงค์ที่ใช้ศึกษาเพิ่มเติม... <br>
[intro to mvc](https://tasty-feet-485.notion.site/Introduction-to-MVC-Comsci-KMITL-654b17725ca942ca8491a649b6fc8d5d) - ตัวนี้มีสอนติดตั้ง node,mysql,dbeaver <br>
[install mysql in linux version](https://tasty-feet-485.notion.site/Install-mysql-on-linux-d1a89d013668437bbd43736cd2551173) - สอนติดตั้ง mysql แบบ linux <br>
[project-automata](https://tasty-feet-485.notion.site/jflex-How-to-compile-and-run-33b1e6cc2d7348b3ab06e72d62e4872c) - โปรเจควิชา automata <br>
