const mysql = require('mysql2');
const connection = mysql.createConnection({
    host: 'localhost',
    database: 'osy2',
    port: 3306,
    user: 'root',
    password: 'admin1234',
});

connection.connect(function(err){
    if(err)
        console.log(err)
    else
        console.log("connect to database")
})

module.exports = connection;