const express = require('express');
const server = express();
const cors = require('cors');
const helmet = require('helmet');
const router = require('./routes/router');
const port = process.env.PORT || 3000;

server.use(cors());
server.use(helmet());
server.use(express.json());
server.use(router);

server.listen(port, () => {
    console.log(`[HOST] http://localhost:${port}/`);
});