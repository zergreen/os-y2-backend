FROM node:16.3.0-alpine

WORKDIR /root/os-y2-backend/

COPY . .
RUN npm i
EXPOSE 3000

CMD node index.js