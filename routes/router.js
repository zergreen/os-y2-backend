const express = require('express');
const router = express.Router();
const endpoint = require('../controller/endpoint');

router.post('/add', endpoint.addEndpoint);

router.get('/query', endpoint.queryEndpoint);

router.get('/truncate', endpoint.truncateEndpoint);

router.get('/gettable', endpoint.getTableEndpoint);

module.exports = router;