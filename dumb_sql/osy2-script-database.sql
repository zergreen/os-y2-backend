CREATE TABLE user (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	student_id VARCHAR(255) ,
	name VARCHAR(255) ,
	sector VARCHAR(255) ,
	province VARCHAR(255),
	hang VARCHAR(255) ,
	county varchar(255),
	road VARCHAR(255),
	house_number VARCHAR(255),
	class VARCHAR(255)
)

select * from user;

-- insert data
INSERT into user values(null,null,null,null,null,null,null,null,null,null);
insert into user(student_id,name) values("63050159","Green");
INSERT into user values(null,"63050160","Phongsakorn","NORTH","BANGKOK","RAMKUMHANG","Soi4",null,"55/2","2")

-- ถ้าเชื่อมด้าต้าเบสไม่ได้ ให้ลอง เปลี่ยน พาสเวิรด์ดู
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'admin1234';
FLUSH PRIVILEGES;
