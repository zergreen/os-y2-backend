const connection = require('../config/db');

module.exports = {
    addPugin: (userModel) => {
        let sql = `INSERT INTO user
        (
            student_id, 
            name,
            sector, 
            province, 
            hang, 
            county, 
            road, 
            house_number, 
            class
        )
        VALUES
        (
             ?, ?, ?, ?, ?, ?, ?, ?, ?
        )`
        connection.query
        (
            sql, 
            [userModel.student_id,
            userModel.name,
            userModel.sector,
            userModel.province,
            userModel.hang,
            userModel.county,
            userModel.road,
            userModel.house_number,
            userModel.class],
            function (err, data) {
                if (err) {
                    console.log(err);
                }
            }
        )
    },

    queryPugin: (res) => {
        let sql = `SELECT * FROM user`
        connection.query
        (
            sql, 
            function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    return res.status(200).send({response: data});
                }
            }
        )
    },

    truncatePugin: (res) => {
        let sql = `TRUNCATE TABLE user;`
        connection.query
        (
            sql, 
            function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    return  res.status(200).send({msg: "reset data successfully"});
                }
            }
        )
    },

    getTablePugin: (res) => {
        let sql = `SELECT * FROM user;`
        connection.query
        (
            sql,
            function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    return res.status(200).send({response: data});
                }
            }
        )
    }
}