const pugin = require('./pugin');
const model = require('../model/model');

module.exports = {
    addLogic: (userModel, res) => {
        for(let i = 1; i < userModel.length; i++) {
            let user = model.userModel
            user.student_id =  userModel[i][0]
            user.name = userModel[i][1]
            user.sector = userModel[i][2]
            user.province = userModel[i][3]
            user.hang = userModel[i][4]
            user.county = userModel[i][5]
            user.road = userModel[i][6]
            user.house_number = userModel[i][7]
            user.class = userModel[i][8]
            console.log(user);
            pugin.addPugin(user);
        }
        res.send({msg : "import data successfully"})
    },

    queryLogic: (res) => {
        pugin.queryPugin(res);
    },
    
    truncateLogic: (res) => {
        pugin.truncatePugin(res);
    },

    getTableLogic: (res) => {
        pugin.getTablePugin(res);
    }
}