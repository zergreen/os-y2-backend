const logic = require('./logic');

const addEndpoint = (req, res) => {
    logic.addLogic(req.body.xlsx, res)
}

const queryEndpoint = (req, res) => {
    logic.queryLogic(res)
}

const truncateEndpoint = (req, res) => {
    logic.truncateLogic(res)
}

const getTableEndpoint = (req, res) => {
    logic.getTableLogic(res)
}

module.exports = {
    addEndpoint,
    queryEndpoint,
    truncateEndpoint,
    getTableEndpoint
}